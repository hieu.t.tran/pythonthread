import threading
import time

def thread1_function(name):
    print("Thread 1 started.")
    for i in range(11):
        print(i)
        time.sleep(1)

def thread2_function(name):
    print("Thread 2 started.")
    for i in range(10,-1,-1):
        print(i)
        time.sleep(1)  

def main():
    x = threading.Thread(target=thread1_function, args=(1,))
    y = threading.Thread(target=thread2_function, args=(2,))
    x.start()
    y.start()
    x.join()
    print("Thread 1 done.")
    y.join()
    print("Thread 2 done.")

main()
